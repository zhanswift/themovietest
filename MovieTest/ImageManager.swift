//
//  ImageManager.swift
//  MovieTest
//
//  Created by Zhanserik on 15.04.2019.
//  Copyright © 2019 JanSwift. All rights reserved.
//

import Kingfisher

class ImageManager {
    
    static let shared = ImageManager()
    
    internal func clearCache() {
        let cache = ImageCache.default
        cache.clearMemoryCache()
        cache.clearDiskCache()
    }
}
