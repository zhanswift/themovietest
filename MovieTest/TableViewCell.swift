//
//  TableViewCell.swift
//  MovieTest
//
//  Created by Jan Su on 14.04.2019.
//  Copyright © 2019 JanSwift. All rights reserved.
//

import UIKit
import Kingfisher

class TableViewCell: UITableViewCell {
    
    let movieNameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 22)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let blackGradient: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "black_gradient")
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let movieImageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleToFill
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let movieToFavorite: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "noActiveStar"), for: .normal)
        button.setImage(UIImage(named: "noActiveStar"), for: .highlighted)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    var movie: Movie? {
        didSet {
            guard let movie = movie else { return }
            movieNameLabel.text = movie.name
            if movie.backdropImage != "" {
                let url = URL(string: movie.backdropImage)
                movieImageView.kf.setImage(with: url)
            } else {
                movieImageView.contentMode = .scaleToFill
                movieImageView.image = UIImage(named: "noImage")
            }
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupView() {
        addSubview(movieImageView)
        addSubview(blackGradient)
        addSubview(movieNameLabel)
        addSubview(movieToFavorite)
        
        movieImageView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        movieImageView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        movieImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        movieImageView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        
        
        blackGradient.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        blackGradient.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        blackGradient.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        blackGradient.heightAnchor.constraint(equalToConstant: 65).isActive = true
        
        movieNameLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10).isActive = true
        movieNameLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        movieNameLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        movieNameLabel.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        movieToFavorite.topAnchor.constraint(equalTo: self.topAnchor, constant: 20).isActive = true
        movieToFavorite.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10).isActive = true
        movieToFavorite.widthAnchor.constraint(equalToConstant: 25).isActive = true
        movieToFavorite.heightAnchor.constraint(equalToConstant: 25).isActive = true
    }
}
