//
//  MainVC + UITableView.swift
//  MovieTest
//
//  Created by Jan Su on 14.04.2019.
//  Copyright © 2019 JanSwift. All rights reserved.
//

import UIKit
import RealmSwift

extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering {
            return filtredMovies.count
        }
        return movies?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID) as! TableViewCell
        var movie: Movie
        if isFiltering {
            movie = filtredMovies[indexPath.row]
        } else {
            guard let mov = movies?[indexPath.row] else { return UITableViewCell() }
            movie = mov
        }
        cell.movie = movie
        cell.movieToFavorite.tag = indexPath.row
        cell.movieToFavorite.addTarget(self, action: #selector(setToFavorite), for: .touchUpInside)
        let idString = String(describing: movie.id)
        let realm = try! Realm()
        if (realm.objects(SavedData.self).filter("favoriteMovieID == %@", idString).first != nil) {
            cell.movieToFavorite.setImage(UIImage(named: "activeStar"), for: .normal)
        } else {
            cell.movieToFavorite.setImage(UIImage(named: "noActiveStar"), for: .normal)
        }
        return cell
    }
    
    @objc func setToFavorite(sender: UIButton) {
        let realm = try! Realm()
        guard let movieID = movies?[sender.tag].id else { return }
        let idString = String(describing: movieID)
        try! realm.write() {
            realm.create(SavedData.self, value: [idString], update: true)
        }
        tableView.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .none)
    }
  
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let movies = movies else { return }
        if indexPath.row == movies.count - 1 {
            print(1)
            page += 1
            manager.getLastMovie(page: page) { [unowned self] inMovie in
                print(2)
                self.movies?.append(inMovie)
                print(3)
                DispatchQueue.main.async {
                    print(4)
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dvc = DetailViewController()
        let movie: Movie!
        if isFiltering {
            movie = filtredMovies[indexPath.row]
        } else {
            guard let mov = movies?[indexPath.row] else { return }
            movie = mov
        }
        dvc.movie = movie
        self.navigationController?.pushViewController(dvc, animated: true)
    }
}
