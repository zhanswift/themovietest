//
//  TableViewController.swift
//  MovieTest
//
//  Created by Jan Su on 14.04.2019.
//  Copyright © 2019 JanSwift. All rights reserved.
//

import UIKit
import RealmSwift

class MainViewController: UIViewController {

    var movies: [Movie]?
    let manager = NetworkManager.shared
    let tableView = UITableView()
    let cellID = "CellID"
    var searchBar: UISearchBar!
    var page = 1
    let searchController = UISearchController(searchResultsController: nil)
    var filtredMovies = [Movie]()
    var isLoading = false
    private var searchBarIsEmpty: Bool {
        guard let text = searchController.searchBar.text else { return false }
        return text.isEmpty
    }
    
    var isFiltering: Bool {
        return searchController.isActive && !searchBarIsEmpty
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        
        movies = []
        
        manager.getLastMovie(page: 1) { [unowned self] inMovie in
            self.movies?.append(inMovie)
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
}
