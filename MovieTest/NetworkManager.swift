//
//  NetworkManager.swift
//  MovieTest
//
//  Created by Jan Su on 14.04.2019.
//  Copyright © 2019 JanSwift. All rights reserved.
//

import Foundation

class NetworkManager {
    static let shared = NetworkManager()
    func getLastMovie(page: Int, callback: @escaping (Movie)->()) {
        
        let date = Date()
        let dateFormmater = DateFormatter()
        dateFormmater.dateFormat = "yyyy-MM-dd"
        let currentDate = dateFormmater.string(from: date)
        
        let urlString = GlobalURL + "discover/movie?api_key=\(API_KEY)&language=\(LanguageLocale)&page=\(page)&media_type=movie&release_date.lte=\(currentDate)&sort_by=release_date.desc"
        
        print(urlString)
        let url = URL(string: urlString)
        let task = URLSession.shared.dataTask(with: url! as URL) { data, response, error in
            guard let data = data, error == nil else { return }
            let temp: NSString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)!
            let myNSData = temp.data(using: String.Encoding.utf8.rawValue)!
            guard let jsonResult = try? JSONSerialization.jsonObject(with: myNSData, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary else {
                return
            }
            if let jsonMovies = jsonResult?["results"] as? [Any] {
                for i in jsonMovies {
                    if let j = i as? [String : Any] {
                        if let desc = j["overview"] as? String {
                            if desc != "" || !desc.isEmpty {
                                let movie = Movie()
                                movie.id = j["id"] as! Int
                                movie.name = j["title"] as! String
                                movie.description = desc
                                if let backdropUrl = j["backdrop_path"] as? String {
                                    movie.backdropImage = ImageURL + backdropUrl
                                }
                                if let posterUrl = j["poster_path"] as? String {
                                    movie.posterImage = ImageURL + posterUrl
                                }
                                callback(movie)
                            }
                        }
                    }
                }
            }
        }
        task.resume()
    }
}
