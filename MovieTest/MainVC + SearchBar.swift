//
//  MainVC + SearchBar.swift
//  MovieTest
//
//  Created by Jan Su on 14.04.2019.
//  Copyright © 2019 JanSwift. All rights reserved.
//

import UIKit


extension MainViewController: UISearchResultsUpdating, UISearchControllerDelegate {
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
    
    private func filterContentForSearchText(_ searchText: String) {
        guard let movies = movies else { return }
        filtredMovies = movies.filter({ (movie: Movie) -> Bool in
            return movie.name.lowercased().contains(searchText.lowercased())
        })
        tableView.reloadData()
    }
}
