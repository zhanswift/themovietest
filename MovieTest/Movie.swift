//
//  Movie.swift
//  MovieTest
//
//  Created by Jan Su on 14.04.2019.
//  Copyright © 2019 JanSwift. All rights reserved.
//

import Foundation

class Movie {
    var id: Int = 0
    var name: String = ""
    var backdropImage: String = ""
    var posterImage: String = ""
    var description: String = ""
}
