//
//  DetailViewController.swift
//  MovieTest
//
//  Created by Jan Su on 14.04.2019.
//  Copyright © 2019 JanSwift. All rights reserved.
//

import UIKit
import Kingfisher

class DetailViewController: UIViewController {

    var movie: Movie!
    
    let movieImageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleToFill
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let movieNameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 22)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let movieDescLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = .black
        label.textAlignment = .justified
        label.font = UIFont.boldSystemFont(ofSize: 17)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white

        self.view.addSubview(movieImageView)
        self.view.addSubview(movieDescLabel)
        
        movieImageView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        movieImageView.heightAnchor.constraint(equalToConstant: 300).isActive = true
        movieImageView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        movieImageView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        
        movieDescLabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 10).isActive = true
        movieDescLabel.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -10).isActive = true
        movieDescLabel.topAnchor.constraint(equalTo: movieImageView.bottomAnchor).isActive = true
        movieDescLabel.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let url = URL(string: movie.posterImage)
        movieImageView.kf.setImage(with: url)
        movieDescLabel.text = movie.description
    }
}
