//
//  SavedData.swift
//  MovieTest
//
//  Created by Jan Su on 14.04.2019.
//  Copyright © 2019 JanSwift. All rights reserved.
//

import RealmSwift

class SavedData: Object {
    
    var favoriteMovieID: String? = nil
    
    override static func primaryKey() -> String? {
        return "favoriteMovieID"
    }
}
