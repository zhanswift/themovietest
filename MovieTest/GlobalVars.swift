//
//  GlobalVars.swift
//  MovieTest
//
//  Created by Jan Su on 14.04.2019.
//  Copyright © 2019 JanSwift. All rights reserved.
//

import Foundation

let GlobalURL = "https://api.themoviedb.org/3/"
let API_KEY = "932b194279fe62e1b40367f25bb7f5ca"
let Language = "ru"
let LanguageLocale = "ru-RU"
let ImageURL = "https://image.tmdb.org/t/p/w500/"
